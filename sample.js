function Vehicle (owner, color, make, model) {          // constructor
    // this = new Bike
      this.owner = owner
      this.color = color
      this.make = make
      this.model = model
    // return new Bike
  }
  
  Vehicle.prototype = {
    constructor: Vehicle,
    start: function (hasKey) {},
    turn: function (direction) {},
    shutdown: function () {
      console.log(this.owner + ' has turned off the ' + this.model)
    },
  }
  
  function Bike (owner, color, make, model, reflectorCount) {
    // this = {}
    Vehicle.call(this, owner, color, make, model) // .call here SETS Vehicle's `this` value to the same as Bike's `this` value
    this.reflectorCount = reflectorCount
  
  
    const clickHandler = function (event) {
      console.log(this)
    }
    document.body.addEventListener('click', clickHandler.bind(this))
  
    // return this
  }
  
  // instance -> Bike.prototype -> Vehicle.prototype -> Object.prototype
  Bike.prototype = Object.create(Vehicle.prototype)
  Bike.prototype.constructor = Bike
  
  Bike.prototype.hellaRamp = function (howHigh) {
    console.log('DUDE, ' + this.owner + '! YOU JUST RAMPED HELLA HIGH! ' + howHigh + ' high!!!')
  }
  
  const eugenesBike = new Bike('Eugene', 'Orange Creamcicle', 'Schwinn', 'Two Eleven', 45)
  const cattyBike = new Bike('Catty', 'Burnt Orange Creamcicle', 'Schwinn', 'Two Eleven', 3)
  const randysBike = new Bike('Randy', 'Red Orange Creamcicle', 'Schwinn', 'Two Eleven', 4)
  const megansBike = new Bike('Megan', 'Sickly Orange Creamcicle', 'Schwinn', 'Two Eleven', 8)
  const bobsBike = new Bike('Bob', 'Orange Creamciclical Reasoning', 'Schwinn', 'Two Eleven', 1)
  
  const myTercel = new Vehicle('dmg', 'red', 'toyota', 'tercel')
  const randysFiesta = new Vehicle('Randy', 'silver', 'Ford', 'Fiesta')
  
  console.log(myTercel)
  console.log(randysFiesta)